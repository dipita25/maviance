<?php

    //entete permettant d'eviter les problèmes de CORS (Cross Origin Resource Sharing)
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: text/plain charset=UTF-8");
    header("Access-Control-Allow-Headers: Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Requested-With");

    /*
        Url de type : localhost/maviance/Rest/ApiListeDevises.php
    */

    require_once './Maviance.php';
        $db = new Maviance();

        $reponse = json_decode($db->isvalidCurrency($_POST['currency']));
        echo ($reponse);
?>